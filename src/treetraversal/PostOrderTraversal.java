package treetraversal;

import java.util.ArrayList;
import java.util.List;


public class PostOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node node) {
		List<Node> result;
		
		if (node == null) {
			return new ArrayList<Node>(); 
		}
		
		result = traverse(node.getLeft());
		result.addAll(traverse(node.getRight()));
		result.add(node);
		
		return result;
	}

}

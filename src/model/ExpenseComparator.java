package model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>{
	
	@Override
	public int compare(Company c1, Company c2) {
		// TODO Auto-generated method stub
		if(c1.getExpenses() < c2.getExpenses())
			return -1;
		else if(c1.getExpenses() > c2.getExpenses())
			return 1;
		else
			return 0;
	}

}

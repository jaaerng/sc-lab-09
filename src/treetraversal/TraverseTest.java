package treetraversal;


public class TraverseTest {

	public static void main(String[] args) {
		Node C = new Node("C", null, null);
		Node E = new Node("E", null, null);
		Node D = new Node("D", C, E);
		Node A = new Node("A", null, null);
		Node B = new Node("B", A, D);
		Node H = new Node("H", null, null);
		Node I = new Node("I", H, null);
		Node G = new Node("G", null, I);
		Node F = new Node("F", B, G);

		ReportConsole report = new ReportConsole();
		
		report.display(F, new PreOrderTraversal());
		report.display(F, new InOrderTraversal());
		report.display(F, new PostOrderTraversal());

		System.out.println("--------------------------");
		
		Node one = new Node("1", null, null);
		Node three = new Node("3", null, null);
		Node two = new Node("2", one, three);
		Node five = new Node("5", null, null);
		Node six = new Node("6", five, null);
		Node four = new Node("4", two, six);
		Node eight = new Node("8", null, null);
		Node ten = new Node("10", null, null);
		Node nine = new Node("9", eight, ten);
		Node seven = new Node("7", four, nine);
				
		report.display(seven, new PreOrderTraversal());
		report.display(seven, new InOrderTraversal());
		report.display(seven, new PostOrderTraversal());
	}

}

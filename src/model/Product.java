package model;


public class Product implements Taxable,Comparable<Product>{
	private String nameProduct;
	private double price;
	
	public Product(String aNamePro, double aPrice){
		this.nameProduct = aNamePro;
		this.price = aPrice;
	}
	
	public String getName(){
		return this.nameProduct;
	}
	
	public double getPrice(){
		return this.price;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return this.price*0.07;
	}

	@Override
	public int compareTo(Product obj) {
		// TODO Auto-generated method stub
		Product other = (Product) obj;
		if(this.getPrice() < other.getPrice())
			return -1;
		else if(this.getPrice() > other.getPrice())
			return 1;
		else
			return 0;
	}
	
	public String toString(){
		return this.nameProduct + " " + this.price ;	
	}
}
